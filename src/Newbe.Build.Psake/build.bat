REM @echo off
%~d0
cd %~dp0
pwsh.exe -NoProfile -ExecutionPolicy unrestricted -Command "& {if(-not (Get-Module -ListAvailable  psake)){Install-Module -Name psake -RequiredVersion 4.8.0 -Scope CurrentUser -Force;}elseif((Get-Module -ListAvailable  psake)[0].Version.ToString() -ne '4.8.0') {Uninstall-Module -Name psake;Install-Module -Name psake -RequiredVersion 4.8.0 -Scope CurrentUser -Force;}invoke-psake .\build.ps1 %*; if ($global:lastexitcode -and $global:lastexitcode -ne 0) {write-host "ERROR CODE: $global:lastexitcode" -fore RED; exit $global:lastexitcode} }"
