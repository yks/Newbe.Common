# 基于Psake进行自动化构建脚本框架

本框架采用 [psake](https://github.com/psake/psake) 作为脚本构建框架，依托 Powershell 本身强大的功能，实现对开发过程自动化的功能。

## V3.0 使用步骤

1. [下载安装netcore 2.2 及以上版本SDK](https://dotnet.microsoft.com/download)
2. [下载安装 Powershell 6](https://github.com/powershell/powershell)
3. 使用控制台或powershell，运行命令`dotnet tool install newbe.build.psake -g`
4. 在需要安装本框架的位置使用控制台或powershell，运行命令`newbe.build.psake init`
5. 按照psake规范对build.ps1进行修改。
6. 执行build.bat便可以执行

## V2.0 使用步骤

1. [下载安装netcore 2.2 及以上版本SDK](https://dotnet.microsoft.com/download)
3. 使用控制台或powershell，运行命令`dotnet tool install newbe.build.psake -g`
4. 在需要安装本框架的位置使用控制台或powershell，运行命令`newbe.build.psake init`
5. 按照psake规范对build.ps1进行修改。
6. 执行build.bat便可以执行

## V1 使用步骤

1. 使用powershell进入你的项目根文件夹。

2. 在powershell中执行以下命令。

  ```powershell
  (curl http://www.newbe.pro/assets/others/scripts/newbe.build.psake.install.txt).Content | powershell
  ```

1. 安装过程需要从互联网下载相关的组件，等待下载完毕即可。
1. 按照psake规范对build.ps1进行修改。
1. 执行build.bat便可以执行
