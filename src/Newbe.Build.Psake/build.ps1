﻿properties {
    $rootNow = Resolve-Path .
    $deployMode = "Release"
    $releaseDir = "$rootNow/Newbe.Build.Psake/build/"
}

# default task
Task Default -depends Build

Task Clean -Description "clean last build result" {
    Remove-Item $releaseDir -Force -Recurse -ErrorAction SilentlyContinue
}

Task Init -depends Clean -Description "init some data" {

}

Task Nuget -depends Init -Description "nuget restore" {
    Exec {
        dotnet restore
    }
}

Task Build -depends Nuget -Description "build sln" {
    Exec {
        dotnet pack -c $deployMode
    }
}

Task Publish -depends Build -description "publish to nuget" {
    Exec{
        Get-ChildItem $releaseDir | foreach {
            dotnet nuget push $_.FullName -s https://www.nuget.org
        }
    }
}
