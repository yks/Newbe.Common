# Newbe.Common

存放Newbe在开发过程中使用到的一些通用工具

## [Newbe.Build.Psake](src/Newbe.Build.Psake/README.md)

基于Psake进行自动化构建脚本框架

## [Newbe.Nuget.LocalStore](src/Newbe.Nuget.LocalStore/README.md)

将本地代码库所依赖的nuget包缓存在特定文件夹，以加快在此安装的速度。
